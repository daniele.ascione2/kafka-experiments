# Kafka Experiments

:woman_scientist: It contains some common patterns when using Kafka Stream.

##  Batch Experiment
:point_right: File [BatchExperiment](./src/main/kotlin/com/masabi/justride/kafka/BatchExperiment.kt) 
How to do batches using kafka stream, processors API and state store