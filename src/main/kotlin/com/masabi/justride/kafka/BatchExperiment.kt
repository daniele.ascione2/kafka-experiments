package com.masabi.justride.kafka

import org.apache.kafka.common.serialization.Serde
import org.apache.kafka.common.serialization.Serdes
import org.apache.kafka.streams.KafkaStreams
import org.apache.kafka.streams.StreamsBuilder
import org.apache.kafka.streams.Topology
import org.apache.kafka.streams.processor.PunctuationType
import org.apache.kafka.streams.processor.api.Processor
import org.apache.kafka.streams.processor.api.ProcessorContext
import org.apache.kafka.streams.processor.api.ProcessorSupplier
import org.apache.kafka.streams.processor.api.Record
import org.apache.kafka.streams.state.KeyValueStore
import org.apache.kafka.streams.state.Stores
import java.time.Duration
import java.util.Properties


const val MAX_BATCH = 100L
val maxDuration: Duration = Duration.ofSeconds(100L)

interface MyBatchOperation<INPUT_RECORD> {

    operator fun invoke(batch: List<INPUT_RECORD>)
}

class BatchExperiment<INPUT_RECORD>(
    private val inputTopic: String,
    properties: Properties,
    private val executeBatchOperation: MyBatchOperation<INPUT_RECORD>
) {

    private val stream: KafkaStreams = KafkaStreams(buildTopology(), properties)

    private val storeBuilder =
        Stores.keyValueStoreBuilder(
            Stores.persistentKeyValueStore("batch-store"),
            Serdes.String(),
            null as Serde<INPUT_RECORD>?
        )

    fun start() {
        stream.start()
    }

    fun stop() {
        stream.close()
    }

    private fun buildTopology(): Topology =
        StreamsBuilder()
            .addStateStore(storeBuilder)
            .applyTopology()
            .build()

    private fun StreamsBuilder.applyTopology(): StreamsBuilder = apply {
        stream<String, INPUT_RECORD>(inputTopic)
            .process(BatchProcessorSupplier(), "batch-store")
    }

    inner class BatchProcessorSupplier : ProcessorSupplier<String, INPUT_RECORD, Void, Void> {
        override fun get(): Processor<String, INPUT_RECORD, Void, Void> =
            object : Processor<String, INPUT_RECORD, Void, Void> {

                private lateinit var stateStoreForBatch: KeyValueStore<String, INPUT_RECORD>

                override fun init(context: ProcessorContext<Void, Void>) {
                    stateStoreForBatch = context.getStateStore("batch-store")

                    context.schedule(maxDuration, PunctuationType.WALL_CLOCK_TIME) {
                        executeBatchOperation(stateStoreForBatch.getAll())
                    }
                }

                override fun process(record: Record<String, INPUT_RECORD>) {
                    stateStoreForBatch.put(record.key(), record.value())

                    if (stateStoreForBatch.approximateNumEntries() >= MAX_BATCH) {
                        executeBatchOperation(stateStoreForBatch.getAll())
                        stateStoreForBatch.deleteAll()
                    }
                }

            }
    }

    private fun KeyValueStore<String, INPUT_RECORD>.getAll() =
        buildList<INPUT_RECORD> { all().forEachRemaining { add(it.value) } }

    private fun KeyValueStore<String, INPUT_RECORD>.deleteAll() {
        all().forEachRemaining { delete(it.key) }
    }
}
